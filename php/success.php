<?php require_once "include/function.php" ?>
<?php includeArea('header'); ?>   
    <div class="main-navigation clearfix">
        <div class="main-navigation__more-info">
            <a href="#">Стать партнером</a>
            <a href="#">Скачать каталог</a>
        </div><!--main-navigation__mre-info-->
        <?php includeArea('search') ?>   
        <div class="main-menu clearfix">
            <div class="main-menu__inner-shadow clearfix">
                <?php includeArea('base-menu'); ?> 
            </div><!--end inner-shadow -->
        </div><!-- end main-navigation-->
    </div><!--end main-navigation-->

    <div class="wrap_sizefix">
        <?php includeArea('breadcrumbs') ?>
        
        <?php includeArea('checkout-step', false, 3) ?>
        
        <?php includeArea('success') ?>

    </div>
    
   

<?php includeArea('footer'); ?> 