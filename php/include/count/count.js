$('.count__inpt').numeric();
$('.count__inpt').keyup(function(){
	if($(this).val() === '0') {
		$(this).val('1' + ' шт.');
	}
});
$('.count__inpt').focusout(function(){
	if($(this).val() === '') {
		$(this).val('1' + ' шт.');
	}
});
$('.count a').click(function(e){
	e.preventDefault();
	var defaultCount = parseInt($(this).parent('.count').find('.count__inpt').val());
	var activeEvent = $(this).attr('data-event');


	if(activeEvent == 'plus') {
		$(this).parent('.count').find('.count__inpt').val(defaultCount+1 + ' шт.');
	} else {
		if(defaultCount != 1) {
			$(this).parent('.count').find('.count__inpt').val((defaultCount-1) + ' шт.');
		}
	}
}); 