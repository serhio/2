<div class="filter">
    <div class="filter__block">
        <a href="#" class="filter__title"><span class="filter__title-name">Для кого</span> <span class="filter__triangle"></span></a>
        <div class="filter__hide">
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt" checked="checked"><span class="filter__field-text">Для женщин</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Для мужчин</span>
        </label>
        </div>
    </div><!--/filter__block-->
    <div class="filter__block">
        <a href="#" class="filter__title"><span class="filter__title-name">Для кого</span> <span class="filter__triangle"></span></a>
        <div class="filter__hide">
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt" checked="checked"><span class="filter__field-text">Новинка</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Классика</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Современный</span>
        </label>
        </div>
    </div><!--/filter__block-->

    <div class="filter__block">
        <a href="#" class="filter__title"><span class="filter__title-name">Семейство</span> <span class="filter__triangle"></span></a>
        <div class="filter__hide">
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt" checked="checked"><span class="filter__field-text">Мягкие цветочные</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Древесно-мховые</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Мягкие восточные</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Цветочно-восточные</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Цитрусовые</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Морские</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Сухие древесные</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Восточные</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Шипровые</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Цветочные</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Древесно-восточные</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Зеленые</span>
        </label>
        <label class="filter__field">
        	<input type="checkbox" class="checkbox__inpt"><span class="filter__field-text">Фужерные</span>
        </label>
        </div>
    </div><!--/filter__block-->
    <input type="submit" class="btn filter__sbmt" value="Показать товары">
</div><!--/filter-->