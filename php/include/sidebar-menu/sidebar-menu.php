<ul class="sidebar-menu">
	 <?php foreach($data as $value) { ?>
      <li class="sidebar-menu__item <?php if($value['current'] === true) echo "sidebar-menu__item--current"?>"><a href="#" class="sidebar-menu__link"><?php echo $value['title']; ?></a><i class="sidebar-menu__triangle <?php if($value['current'] === true) echo "sidebar-menu__triangle--open"?>"></i>
		<?php
			if($value['submenu']) {
		?>
			<ul class="sidebar-menu__dropdown">
				<?php
					foreach($value['submenu'] as $item) {
				?>
					<!-- активному элементу добавляем класс  sidebar-menu__dropdown-link--current -->
					<li class="sidebar-menu__dropdown-item"><a href="#" class="sidebar-menu__dropdown-link"><?php echo $item; ?></a></li>
				<?php
					}
				?>
			</ul>
		<?php
			}
		?>
	  </li>
   <?php } ?>
</ul>