<div class="order-history">
	<div class="order-histiry__item">
		<div class="order-histiry__item-head clearfix">
			<div class="order-history__num">Заказ <span>№94</span> от 10.12.2014<br><span class="order-history__time">10:25:47</span></div>
			<span class="order-history__status order-history__status--orange">Принят, ожидается оплата</span>
		</div>
		<div class="order-histiry__item-body clearfix">
			<div class="order-histiry__coll">
				<h3>Сумма:</h3>
				<p>35 000 р. (не оплачен)</p>
			</div>
			<div class="order-histiry__coll">
				<h3>Способ оплаты:</h3>
				<p>Наличными курьеру</p>
			</div>
			<div class="order-histiry__coll">
				<h3>Способ доставки:</h3>
				<p>Москва (в пределах МКАД)</p>
			</div>

			<div class="order-histiry__coll--small">
				<a href="#" class="order-histiry__link--blue">Повторить заказ</a>
				<br><a href="#" class="order-histiry__link--red">Отменить заказ</a>
			</div>
			<div class="clearfix"></div>
			<h3 class="order-histiry__title-small">Состав заказа:</h3>
			<ol class="order-histiry__list">
				<li><a href="#">Наименование конкретного товара может быть длинным, не длинее одной строки</a></li>
				<li><a href="#">Наименование конкретного товара может быть длинным, не длинее одной строки</a></li>
				<li><a href="#">Наименование конкретного товара может быть длинным, не длинее одной строки</a></li>
			</ol>
			<a href="#" class="btn order-histiry__btn">Подробнее</a>
		</div>
	</div>

	<div class="order-histiry__item">
		<div class="order-histiry__item-head clearfix">
			<div class="order-history__num">Заказ <span>№94</span> от 10.12.2014<br><span class="order-history__time">10:25:47</span></div>
			<span class="order-history__status order-history__status--green">Принят, оплачен</span>
		</div>
		<div class="order-histiry__item-body clearfix">
			<div class="order-histiry__coll">
				<h3>Сумма:</h3>
				<p>35 000 р. (не оплачен)</p>
			</div>
			<div class="order-histiry__coll">
				<h3>Способ оплаты:</h3>
				<p>Наличными курьеру</p>
			</div>
			<div class="order-histiry__coll">
				<h3>Способ доставки:</h3>
				<p>Москва (в пределах МКАД)</p>
			</div>

			<div class="order-histiry__coll--small">
				<a href="#" class="order-histiry__link--blue">Повторить заказ</a>
				<br><a href="#" class="order-histiry__link--red">Отменить заказ</a>
			</div>
			<div class="clearfix"></div>
			<h3 class="order-histiry__title-small">Состав заказа:</h3>
			<ol class="order-histiry__list">
				<li><a href="#">Наименование конкретного товара может быть длинным, не длинее одной строки</a></li>
				<li><a href="#">Наименование конкретного товара может быть длинным, не длинее одной строки</a></li>
				<li><a href="#">Наименование конкретного товара может быть длинным, не длинее одной строки</a></li>
			</ol>
			<a href="#" class="btn order-histiry__btn">Подробнее</a>
		</div>
	</div>
</div>