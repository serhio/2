<ul class="catalog-list <?php if($mod) echo "catalog-list--". $mod ?>">
	<?php
		$i = 1;
		foreach ($data as $value) {
    ?>
		<li class="catalog-list__item <?php if($i % $par1 === 0) echo 'catalog-list__item--last' ?>">
			<div class="catalog-list__hover-block">
				<a href="#fast-view" class="catalog-list__preview">Быстрый просмотр</a>
				<div class="catalog-list__add-cart-wrap">
					<a href="#" class="catalog-list__add-cart">В корзину</a>
					<a href="#" class="catalog-list__add-faforite"><i class="catalog-list__add-faforite-icon icon-favorite"></i></a>
				</div>
			</div>
			<div class="catalog-list__flag">
				<?php if ($value['new']) {?>
					<span class="catalog-list__flag-item icon-new"></span>
				<?php };
				 if ($value['favorite']) {?>
					<span class="catalog-list__flag-item icon-fav"></span>
				<?php } ?>
			</div>
			<a href="#" class="catalog-list__link">
				<span class="catalog-list__thumb"><img src="<?=$value['src']; ?>" alt="" class="catalog-list__thumb-img"></span>
				<span class="catalog-list__title"><?=$value['title']; ?></span>
				<span class="catalog-list__descr"><?=$value['descr']; ?></span>
				<span class="catalog-list__price"><?=$value['price']; ?> руб. <i class="catalog-list__old-price"><?=$value['oldPrice']; ?> руб.</i></span>
			</a>
		</li>
    <?php
    	$i++;
    	}
    ?>
</ul>
<!--/catalog-list -->