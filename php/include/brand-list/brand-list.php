<div class="brand-list">
	<h2 class="brand-list__title">Бренды парфюмерии:</h2>
	<ul class="brand-list__alphabet">
		<li class="brand-list__alphabet-item">A</li>
		<li class="brand-list__alphabet-item">B</li>
		<li class="brand-list__alphabet-item">C</li>
		<li class="brand-list__alphabet-item">D</li>
		<li class="brand-list__alphabet-item">E</li>
		<li class="brand-list__alphabet-item">F</li>
		<li class="brand-list__alphabet-item">G</li>
		<li class="brand-list__alphabet-item">H</li>
		<li class="brand-list__alphabet-item">I</li>
		<li class="brand-list__alphabet-item">J</li>
		<li class="brand-list__alphabet-item">K</li>
		<li class="brand-list__alphabet-item brand-list__alphabet-item--current">L</li>
		<li class="brand-list__alphabet-item">M</li>
		<li class="brand-list__alphabet-item">N</li>
		<li class="brand-list__alphabet-item">O</li>
		<li class="brand-list__alphabet-item">P</li>
		<li class="brand-list__alphabet-item">Q</li>
		<li class="brand-list__alphabet-item">R</li>
		<li class="brand-list__alphabet-item">S</li>
		<li class="brand-list__alphabet-item">T</li>
		<li class="brand-list__alphabet-item">U</li>
		<li class="brand-list__alphabet-item">V</li>
		<li class="brand-list__alphabet-item">W</li>
		<li class="brand-list__alphabet-item">X</li>
		<li class="brand-list__alphabet-item">Y</li>
		<li class="brand-list__alphabet-item">Z</li>
	</ul>
	<div class="brand-list__content clearfix">
		<ul class="brand-list__coll">
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">La Perla</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">La Prairie</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Lacoste</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Lady Gaga</a></li>
		</ul>

		<ul class="brand-list__coll">
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Lalique Parfums</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Lancome</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Lanvin</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">L'Artisan Parfumeur</a></li>
		</ul>

		<ul class="brand-list__coll">
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Laura Biagiotti</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Les Parfums Molyneux</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Liz Claiborne</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Lobogal</a></li>
		</ul>
		<ul class="brand-list__coll">
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Loewe</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Lolita Lempicka</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Lomani</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Louis Feraud</a></li>
		</ul>

		<ul class="brand-list__coll">
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Lubin</a></li>
			<li class="brand-list__item"><a href="#" class="brand-list__item-link">Lulu Castagnette</a></li>
		</ul>
	</div>
</div>