<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="../css/reset.css" />
<link rel="stylesheet" href="../css/sprites.css" /> 
<link rel="stylesheet" href="../css/result.css" /> 

<link rel="stylesheet" href="../lib/chosen.css" />
<link rel="stylesheet" href="../lib/pikachoose/right.css" />
<link rel="stylesheet" href="../lib/fancybox/jquery.fancybox.css" />
<title>Parfum</title>
<meta name="viewport" content="width=1094" />
<!--[if lt IE 9]>
   <script>
      document.createElement('header');
      document.createElement('section');
      document.createElement('footer');
      document.createElement('nav');
      document.createElement('article');
      document.createElement('aside');
   </script>
<![endif]-->

</head>
<body>
	 <?php includeArea('top-panel'); ?>
   <div class="wrapper <?php if($mod) echo "wrapper--" . $mod ?>">
    <header class="header clearfix">
        <div class="header__sizefix clearfix">
            <?php includeArea('logo'); ?> 
            
            <?php includeArea('address'); ?> 
            
            <?php includeArea('currency'); ?> 
            
            <?php includeArea('header-menu'); ?> 
        </div><!-- end container -->
    </header>