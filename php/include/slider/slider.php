<div class="slider clearfix">
    <ul class="slide__list">
        <li><a href="#"><img src="../upload/slide1.jpg" alt="" /></a></li>
        <li><a href="#"><img src="../upload/slide2.jpg" alt="" /></a></li>
        <li><a href="#"><img src="../upload/slide3.jpg" alt="" /></a></li>
        <li><a href="#"><img src="../upload/slide4.jpg" alt="" /></a></li>
    </ul>
    <div class="slider__caption-bg"></div>
    <ul class="slider__pagination">
        <li>
            <div class="caption">
                <p>Текст баннера, распродажи
                или любого другого предложения.
                Тест написан для примера.</p>
            </div>
            <span>Про фессиональные
            консультанты</span></li>
        <li>
            <div class="caption">
                <p>Текст баннера, распродажи
                или любого другого предложения.
                Тест написан для примера.</p>
            </div>
            <span>Доставка в любую
            точку России</span></li>
        <li>
            <div class="caption">
                <p>Текст баннера, распродажи
                или любого другого предложения.
                Тест написан для примера.</p>
            </div>
            <span>Быстрое решение
            по кредиту</span></li>
        <li>
            <div class="caption">
                <p>Текст баннера, распродажи
                или любого другого предложения.
                Тест написан для примера.</p>
            </div>
            <span>Гибкая система
            скидок</span></li>
    </ul>
    
</div><!-- end parfum-slider-wrap -->