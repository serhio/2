<div class="pagination">
	<a href="#" class="pagination__item pagination__text">Предыдущая страница</a>
	<a href="#" class="pagination__item pagination__num">1</a>
	<a href="#" class="pagination__item pagination__num">2</a>
	<a href="#" class="pagination__item pagination__num">3</a>
	<a href="#" class="pagination__item pagination__num">4</a>
	<a href="#" class="pagination__item pagination__num pagination__num--current">5</a>
	<a href="#" class="pagination__item pagination__num">6</a>
	<a href="#" class="pagination__item pagination__num">7</a>
	<a href="#" class="pagination__item pagination__num">8</a>
	<a href="#" class="pagination__item pagination__num">9</a>
	<a href="#" class="pagination__item pagination__num">10</a>
	<a href="#" class="pagination__item pagination__text">Следующая страница</a>
</div>