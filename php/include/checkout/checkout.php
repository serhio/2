<div class="checkout">
	<h2 class="checkout__title">Контактные данные</h2>
	<label class="checkout__label">Ваше имя</label>
	<input type="text" class="checkout__inpt text-field">
	<label class="checkout__label">E-mail</label>
	<input type="text" class="checkout__inpt text-field">
	<label class="checkout__label">Ваш город</label>
	<input type="text" class="checkout__inpt text-field">
	<label class="checkout__label">Контактный телефон</label>
	<input type="text" class="checkout__inpt checkout__inpt--phone text-field">

	<p class="checkout__tab">Хочу оформить как  <a href="#fiz" class="checkout__tab-link checkout__tab-link--current">Физическое лицо</a>  или  <a href="#ur" class="checkout__tab-link">Юридическое лицо</a></p>
	<div class="" id="fiz">
	<div class="checkout__radio-wrap">
		<label class="checkout__radio">
			<input type="radio" name="delivery" class="checkout__radio-inpt" checked="checked">
			<span class="checkout__radio-txt">Самовывоз</span>
		</label>
		<label class="checkout__radio">
			<input type="radio" name="delivery" class="checkout__radio-inpt">
			<span class="checkout__radio-txt">Доставка курьером</span>
		</label>
		<label class="checkout__radio">
			<input type="radio" name="delivery" class="checkout__radio-inpt">
			<span class="checkout__radio-txt">Почта России</span>
		</label>
	</div>
	<label class="checkout__label">Адрес доставки</label>
	<textarea class="text-field checkout__textarea"></textarea>
	
	</div>
	<h2 class="checkout__title-small">Способ оплаты</h2>
	<div class="checkout__radio-wrap">
		<label class="checkout__radio">
			<input type="radio" name="payment" class="checkout__radio-inpt" checked="checked">
			<span class="checkout__radio-txt">Наличными</span>
		</label>
		<label class="checkout__radio">
			<input type="radio" name="payment" class="checkout__radio-inpt">
			<span class="checkout__radio-txt">Банковская карта</span>
		</label>
		<label class="checkout__radio">
			<input type="radio" name="payment" class="checkout__radio-inpt">
			<span class="checkout__radio-txt">Наложным платежом</span>
		</label>
	</div>

	<h2 class="checkout__title">Состав заказа</h2>
	<table class="checkout-cart">
		<thead>
			<tr>
				<th class="checkout-cart__head checkout-cart__head--photo">Фото</th>
				<th class="checkout-cart__head checkout-cart__head--name">Название</th>
				<th class="checkout-cart__head checkout-cart__head--count">Количество</th>
				<th class="checkout-cart__head">Стоимость</th>
			</tr>
		</thead>
		<tr>
			<td class="checkout-cart__item"><a href="#" class="checkout-cart__thumb"><img src="../upload/good1.jpg" alt=""></a></td>
			<td class="checkout-cart__item"><a href="#" class="checkout-cart__name">Наименование марки парфюма может быть длиной в две строки, если наименование очень длинное</a>
			<span class="checkout-cart__art"><span>Артикул:</span> 626113</span>
			</td>
			<td class="checkout-cart__item checkout-cart__item--count">20</td>
			<td class="checkout-cart__item checkout-cart__item--price">12 999 р.</td>
		</tr>
		<tr>
			<td class="checkout-cart__item"><a href="#" class="checkout-cart__thumb"><img src="../upload/good1.jpg" alt=""></a></td>
			<td class="checkout-cart__item"><a href="#" class="checkout-cart__name">Наименование марки парфюма может быть длиной в две строки, если наименование очень длинное</a>
			<span class="checkout-cart__art"><span>Артикул:</span> 626113</span>
			</td>
			<td class="checkout-cart__item checkout-cart__item--count">20</td>
			<td class="checkout-cart__item checkout-cart__item--price">12 999 р.</td>
		</tr>
		<tr>
			<td class="checkout-cart__item"><a href="#" class="checkout-cart__thumb"><img src="../upload/good1.jpg" alt=""></a></td>
			<td class="checkout-cart__item"><a href="#" class="checkout-cart__name">Наименование марки парфюма может быть длиной в две строки, если наименование очень длинное</a>
			<span class="checkout-cart__art"><span>Артикул:</span> 626113</span>
			</td>
			<td class="checkout-cart__item checkout-cart__item--count">20</td>
			<td class="checkout-cart__item checkout-cart__item--price">12 999 р.</td>
		</tr>
	</table>
	<div class="checkout__bottom clearfix">
		<div class="checkout__comment">
		<label class="checkout__label">Дополнительная информация к заказу</label>
		<textarea class="text-field checkout__textarea"></textarea>
		</div>

		<div class="cart__total-bottom">
		<p><span>Итого:</span>  150 000 р.</p>
		<a href="#" class="btn checkout__btn">Оформить заказ</a>
	</div>
	</div>
</div>