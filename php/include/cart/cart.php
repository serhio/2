<div class="cart clearfix">
	<div class="cart__head clearfix">
		<div class="cart__head-item cart__head-item--photo">
			Фото
		</div>
		<div class="cart__head-item cart__head-item--name">
			Название
		</div>
		<div class="cart__head-item cart__head-item--count">
			Количество
		</div>
		<div class="cart__head-item cart__head-item--price">
			Цена
		</div>
		<div class="cart__head-item cart__head-item--total">
			Стоимость
		</div>
	</div>
	<?php 
        $cartItem = array(
            array(
                'src'      => '../upload/good1.jpg',
                'title'    => 'Наименование марки парфюма',
                'descr'    => 'Наименование конкретного продукта',
                'price'    => '12 024',
                'oldPrice' => '13 400'
            ),
            array(
                'src'      => '../upload/good2.jpg',
                'title'    => 'Наименование марки парфюма может быть длиной в три строки',
                'descr'    => 'Наименование конкретного продукта',
                'price'    => '12 024',
                'oldPrice' => '13 400'
            ),
            array(
                'src'      => '../upload/good3.jpg',
                'title'    => 'Наименование марки парфюма',
                'descr'    => 'Наименование конкретного продукта',
                'price'    => '12 024',
                'oldPrice' => '13 400'
            )
        );

        foreach ($cartItem as $value) {
    ?>
		<div class="cart__item clearfix">
			<a href="#" class="cart__thumb">
				<img src="<?php echo $value['src'];?>" alt="<?php echo $value['title'];?>">
			</a>

			<div class="cart__descr">
				<a href="#" class="cart__descr-name"><?php echo $value['title'];?></a>
				<p class="cart__descr-caption"><?php echo $value['descr'];?></p>
				<span class="cart__descr-art">Артикул: <i>626113</i></span>
			</div>

			<div class="cart__count">
				<?php includeArea('count'); ?>
			</div>
			<div class="cart__price">
				<?php echo $value['price'];?> р.
				<span class="cart__oldprice"><?php echo $value['oldPrice'];?> руб.</span>
			</div>
			<div class="cart__total">
				<span class="cart__total-summ">12 999 р.</span>
				<a href="#" class="cart__link">Отложить</a>
			</div>
			<div class="cart__delete">
				<a href="#" class="cart__delete-link icon-delete"></a>
			</div>
		</div>
    <?php
        }
    ?>
	
	<div class="cart__promocode">
		<h2>Скидки и бонусы</h2>
		<input type="text" class="text-field" placeholder="Впишите промо-код здесь">
		<input type="submit" value="Применить" class="btn btn--blue">
	</div>

	<div class="cart__total-bottom">
		<p><span>Итого:</span>  150 000 р.</p>
		<a href="#" class="btn">Перейти к оформлению</a>
	</div>
</div>