<div class="success clearfix">
	<h1 class="success__title">Ваш заказ <span>№3504210</span> успешно создан</h1>
	<div class="success__col1">
		<h3 class="success__title-small">Дата оформления:</h3>
		<p>20.12. 2013 г.</p>

		<h3 class="success__title-small">Способ оплаты:</h3>
		<p>наличными курьеру</p>

		<h3 class="success__title-small">Сумма заказа:</h3>
		<p>20.12. 2013 г.</p>
		<h3 class="success__title-small">Статус:</h3>
		<p>Успешно принят!</p>
	</div>

	<div class="success__col2">
		<h3 class="success__title-small">Следить за заказом</h3>
		<p>Вы автоматически авторизировались на сайте
и теперь сможете следить за выполнением заказа
в личном кабинете сайта.
<br>Обратите внимание, что для входа в этот раздел вам необходимо будет ввести логин и пароль пользователя сайта.</p>

<h3 class="success__title-small">Внимание!</h3>
<p>Пароль сгенерировался автоматически.
Измените его в <a href="#" class="success__link">специальном разделе</a>.</p>

<a href="#" class="btn btn--blue success__btn">Распечатать данные о заказе</a>
	</div>
	<div class="success__col3">
		<h3 class="success__title-small">Подтверждение заказа</h3>
<p>Менеджер свяжется с вами
в течение 20 минут,
для подтверждения заказа.</p>
	</div>
</div>