<div class="lk clearfix">
	<div class="lk__left">
		<?php 
				$itemMenu = array(
					array(
						"title"   => "Личная информация",
						"current" => true,
						"submenu" => array("Изменить регистрационные данные")
					),
					array(
						"title"   => "Заказы",
						"current" => false,
						"submenu" => array("Состояние заказов", "История заказов")
					),
					array(
						"title"   => "Подписка",
						"current" => false,
						"submenu" => false
					)
				);
				includeArea('sidebar-menu', false, $itemMenu ); 

			?>
	</div>

	<div class="lk__content">
		<?php if($mod =='start') { ?>
		<p>В личном кабинете Вы можете проверить текущее состояние корзины, ход выполнения Ваших заказов, просмотреть или изменить личную информацию, а также подписаться на новости и другие информационные рассылки.</p>
		<?php } elseif ($mod == 'reg') { 
			includeArea('reg'); 
		} elseif ($mod === 'order-history') {
			includeArea('order-history'); 
		} ?>
	</div>
</div>