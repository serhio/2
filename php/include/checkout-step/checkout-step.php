<div class="checkout-step">
	<span class="checkout-step__item <?php if($data === 1) echo 'checkout-step__item--current' ?>">Корзина</span>
	<span class="checkout-step__arrow icon-arrow-cart"></span>
	<span class="checkout-step__item <?php if($data === 2) echo 'checkout-step__item--current' ?>">Оформление</span>
	<span class="checkout-step__arrow icon-arrow-cart"></span>
	<span class="checkout-step__item <?php if($data === 3) echo 'checkout-step__item--current' ?>">Заказ готов</span>
</div>