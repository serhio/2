<div class="reg">
	<form action="#">
		<p class="checkout__tab reg__tab">Хочу зарегестрироваться как <a href="#fiz" class="checkout__tab-link checkout__tab-link--current">Физическое лицо</a>  или  <a href="#ur" class="checkout__tab-link">Юридическое лицо</a></p>
		<label class="reg__label">Ваше имя <span>*</span></label>
		<input type="text" class="text-field reg__inpt">
		<label class="reg__label">E-mail <span>*</span></label>
		<input type="text" class="text-field reg__inpt">

		<label class="reg__label">Пароль <span>*</span></label>
		<input type="password" class="text-field reg__inpt reg__inpt--small">

		<label class="reg__label">Повторите пароль <span>*</span></label>
		<input type="password" class="text-field reg__inpt reg__inpt--small">

		<label class="reg__label">Контактный телефон</label>
		<input type="text" class="text-field reg__inpt reg__inpt--small">
		
		<label class="reg__label">Введите код с картинки <span>*</span></label>
		<div class="reg__captcha">
			<img src="../upload/capthca.png" alt="">
			<a href="#" class="reg__refresh icon-refresh"></a>
			<input type="text" class="text-field reg__inpt reg__inpt--captcha">
		</div>
		<input type="submit" value="Зарегестрироваться" class="btn reg__btn">
		<p class="reg__social">Или зайдите при помощи соц. сетей <a href="#" class="reg__social-link icon-vk"></a> <a href="#" class="reg__social-link icon-fb"></a></p>
	</form>
</div>