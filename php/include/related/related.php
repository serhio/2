<div class="related">
	<h2 class="related__title">С этим товаром также покупают:</h2>
	<?php 
                    $newGoods = array(
                        array(
                            'src'      => '../upload/good1.jpg',
                            'title'    => 'Наименование марки парфюма',
                            'descr'    => 'Наименование конкретного продукта',
                            'price'    => '12 024',
                            'oldPrice' => '13 400',
                            'new'      => true,
                            'favorite' => false
                        ),
                        array(
                            'src'      => '../upload/good2.jpg',
                            'title'    => 'Наименование марки парфюма может быть длиной в три строки',
                            'descr'    => 'Наименование конкретного продукта',
                            'price'    => '12 024',
                            'oldPrice' => '13 400',
                            'new'      => false,
                            'favorite' => false
                        ),
                        array(
                            'src'      => '../upload/good3.jpg',
                            'title'    => 'Наименование марки парфюма',
                            'descr'    => 'Наименование конкретного продукта',
                            'price'    => '12 024',
                            'oldPrice' => '13 400',
                            'new'      => false,
                            'favorite' => true
                        ),
                        array(
                            'src'      => '../upload/good4.jpg',
                            'title'    => 'Наименование марки парфюма может быть длиной в три строки',
                            'descr'    => 'Наименование конкретного продукта',
                            'price'    => '12 024',
                            'oldPrice' => '13 400',
                            'new'      => false,
                            'favorite' => false
                        )
                    );
                ?>
                <?php includeArea('catalog-list', 'related', $newGoods, 4) ?>
</div>