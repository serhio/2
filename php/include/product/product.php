<div class="product clearfix <?php if($mod) echo 'product--' . $mod ?>">
	<div class="product__left">
		<ul class="product__gallery jcarousel-skin-pika">
			<li><img src="../upload/prdct1.jpg" alt=""></li>
			<li><img src="../upload/prdct1.jpg" alt=""></li>
			<li><img src="../upload/prdct1.jpg" alt=""></li>
			<li><img src="../upload/prdct1.jpg" alt=""></li>
		</ul> 
	</div>

	<div class="product__right">
	<?php if($mod == 'fast-view') { ?>
		<h2 class="product__title--fast-view">Наименование марки парфюма длиной в две строки</h2>
	<?php } ?>
		<div class="product__price">2 999 р. <span class="product__oldprice">3 400 руб.</span></div>
		<h3 class="product__count">Количество:</h3>
		<?php includeArea('count') ?>
		<div>
			<a href="#" class="btn product__addcart">В корзину</a>
			
			

			<h3 class="product__title">Другие емкости:</h3>
			<div class="product__item">
				<a href="#" class="product__name">Eau De Lacoste туалетные духи 30ml</a>
				<a href="#" class="product__item-addcart">В корзину</a> <span class="product__item-price">1 000 р.</span><a href="#" class="product__item-favorite">Отложить</a>
			</div>

			<div class="product__item">
				<a href="#" class="product__name">Eau De Lacoste набор 2пр туалетные духи 50ml туалетные духи 50ml</a>
				<a href="#" class="product__item-addcart">В корзину</a> <span class="product__item-price">13 500 р.</span><a href="#" class="product__item-favorite">Отложить</a>
			</div>

			<div class="product__item">
				<a href="#" class="product__name">Eau De Lacoste туалетные духи 30ml</a>
				<a href="#" class="product__item-addcart">В корзину</a> <span class="product__item-price">3 499 р.</span><a href="#" class="product__item-favorite">Отложить</a>
			</div>
			<img src="../upload/like.png" alt="" style="margin: 20px 0 0;">
		</div>
	</div>
</div>