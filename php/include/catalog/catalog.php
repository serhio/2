<div class="catalog clearfix">
        
        <div class="catalog__left">
            <?php includeArea('filter') ?>
        </div><!--/catalog-left-->
        <div class="catalog__content">
            <?php includeArea('sort') ?>
            <?php 
            $goods = array(
                array(
                    'src'      => '../upload/good1.jpg',
                    'title'    => 'Наименование марки парфюма',
                    'descr'    => 'Наименование конкретного продукта',
                    'price'    => '12 024',
                    'oldPrice' => '13 400',
                    'new'      => true,
                    'favorite' => false
                ),
                array(
                    'src'      => '../upload/good2.jpg',
                    'title'    => 'Наименование марки парфюма может быть длиной в три строки',
                    'descr'    => 'Наименование конкретного продукта',
                    'price'    => '12 024',
                    'oldPrice' => '13 400',
                    'new'      => false,
                    'favorite' => false
                ),
                array(
                    'src'      => '../upload/good3.jpg',
                    'title'    => 'Наименование марки парфюма',
                    'descr'    => 'Наименование конкретного продукта',
                    'price'    => '12 024',
                    'oldPrice' => '13 400',
                    'new'      => false,
                    'favorite' => true
                ),
                array(
                    'src'      => '../upload/good4.jpg',
                    'title'    => 'Наименование марки парфюма может быть длиной в три строки',
                    'descr'    => 'Наименование конкретного продукта',
                    'price'    => '12 024',
                    'oldPrice' => '13 400',
                    'new'      => false,
                    'favorite' => false
                ),
                array(
                    'src'      => '../upload/good1.jpg',
                    'title'    => 'Наименование марки парфюма',
                    'descr'    => 'Наименование конкретного продукта',
                    'price'    => '12 024',
                    'oldPrice' => '13 400',
                    'new'      => true,
                    'favorite' => false
                ),
                array(
                    'src'      => '../upload/good2.jpg',
                    'title'    => 'Наименование марки парфюма может быть длиной в три строки',
                    'descr'    => 'Наименование конкретного продукта',
                    'price'    => '12 024',
                    'oldPrice' => '13 400',
                    'new'      => false,
                    'favorite' => false
                ),
                array(
                    'src'      => '../upload/good3.jpg',
                    'title'    => 'Наименование марки парфюма',
                    'descr'    => 'Наименование конкретного продукта',
                    'price'    => '12 024',
                    'oldPrice' => '13 400',
                    'new'      => false,
                    'favorite' => true
                ),
                array(
                    'src'      => '../upload/good4.jpg',
                    'title'    => 'Наименование марки парфюма может быть длиной в три строки',
                    'descr'    => 'Наименование конкретного продукта',
                    'price'    => '12 024',
                    'oldPrice' => '13 400',
                    'new'      => false,
                    'favorite' => false
                ),array(
                    'src'      => '../upload/good1.jpg',
                    'title'    => 'Наименование марки парфюма',
                    'descr'    => 'Наименование конкретного продукта',
                    'price'    => '12 024',
                    'oldPrice' => '13 400',
                    'new'      => true,
                    'favorite' => false
                )
            );
        ?>
        <?php includeArea('catalog-list', false, $goods, 3) ?>

        <?php includeArea('pagination') ?>
        </div><!--/catalog-content-->
    </div>