<?php includeArea('footer-top'); ?> 
</div>
<footer class="footer">
            
            <?php includeArea('footer-menu'); ?> 
            
            <div class="footer__sizefix">
            <div class="footer__center clearfix">
                <div class="footer__address">
                    <?php includeArea('order-call', 'footer'); ?> 
                    <?php includeArea('phone'); ?> 

                    <?php includeArea('social'); ?> 
                </div>
                
                <?php
                    $footerMenuBlock1 = array(
                        array(
                            "title" => "Как сделать заказ",
                            "large" => true
                        ),
                        array(
                            "title" => "Способ доставки",
                            "large" => false
                        ),
                        array(
                            "title" => "Способ оплаты",
                            "large" => false
                        )
                    );
                    $footerMenuBlock2 = array(
                        array(
                            "title" => "Оплата и доставка",
                            "large" => true
                        ),
                        array(
                            "title" => "Обмен и возврат",
                            "large" => false
                        ),
                        array(
                            "title" => "Гарантия",
                            "large" => false
                        ),
                        array(
                            "title" => "Доставка",
                            "large" => false
                        )
                    );
                    $footerMenuBlock3 = array(
                        array(
                            "title" => "О компании",
                            "large" => true
                        ),
                        array(
                            "title" => "Наши клиенты",
                            "large" => false
                        ),
                        array(
                            "title" => "Наши поставщики",
                            "large" => false
                        ),
                        array(
                            "title" => "Новости",
                            "large" => false
                        )
                    );
                    $footerMenuBlock4 = array(
                        array(
                            "title" => "Партнерство",
                            "large" => true
                        ),
                        array(
                            "title" => "Физ. и Юр. лицам",
                            "large" => false
                        ),
                        array(
                            "title" => "Как стать партнером",
                            "large" => false
                        ),
                        array(
                            "title" => "Типовой договор",
                            "large" => false
                        )
                    );
                    $footerMenuBlock5 = array(
                        array(
                            "title" => "Контакты",
                            "large" => true
                        ),
                        array(
                            "title" => "Заказать звонок",
                            "large" => false
                        ),
                        array(
                            "title" => "Обратная связь",
                            "large" => false
                        )
                    );
                    includeArea('footer-menu-block', false, $footerMenuBlock1);
                    includeArea('footer-menu-block', false, $footerMenuBlock2);
                    includeArea('footer-menu-block', false, $footerMenuBlock3);
                    includeArea('footer-menu-block', false, $footerMenuBlock4);
                    includeArea('footer-menu-block', 'last', $footerMenuBlock5);
                ?>
            </div>

                <div class="footer__bottom clearfix">
                    <?php includeArea('copy'); ?>
                    <?php includeArea('shild'); ?>
                </div><!--end footer__bottom-->
            </div>
    </footer>
    
    <div id="fast-view" style="display: none;">
        <?php includeArea('product', 'fast-view') ?>
    </div>
    <div id="form-call" style="display: none;">
        <?php includeArea('form-call') ?>
    </div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../lib/parfumSlider.js"></script>
    <script src="../lib/checkbox.js"></script>
    <script src="../lib/radio.js"></script>
    <script src="../lib/chosen.jquery.js"></script>
    <script src="../lib/pikachoose/jquery.pikachoose.js"></script>
    <script src="../lib/pikachoose/jquery.jcarousel.min.js"></script>
    <script src="../lib/jquery.numeric.min.js"></script>
    <script src="../lib/fancybox/jquery.fancybox.js"></script> 
	<script src="../js/script.js"></script>
</body>
</html> 