<ul class="footer-menu-block <?php if($mod) echo "footer-menu-block--" . $mod ?>">
	<?php 
	foreach($data as $value) { ?>
		<li class="footer-menu-block__item"><a href="#" class="footer-menu-block__link <?php if($value['large']) echo "footer-menu-block__link--large"; ?>"><?php echo $value['title']; ?></a></li>
	<?php 
		}
	?>
</ul>