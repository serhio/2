<?php require_once "include/function.php" ?>
<?php includeArea('header'); ?>   
    <div class="main-navigation clearfix">
        <div class="main-navigation__more-info">
            <a href="#">Стать партнером</a>
            <a href="#">Скачать каталог</a>
        </div><!--main-navigation__mre-info-->
        <?php includeArea('search') ?>   
        <div class="main-menu clearfix">
            <div class="main-menu__inner-shadow clearfix">
                <?php includeArea('base-menu'); ?> 
            </div><!--end inner-shadow -->
        </div><!-- end main-navigation-->
    </div><!--end main-navigation-->

    <div class="wrap_sizefix">
        <?php includeArea('breadcrumbs') ?>
        <h1 class="title">Lacoste</h1>
        
        <div class="description">
            <img src="../upload/logo1.png" style="margin: 0 20px 0 0" align="left">
            <p>В самых простых вещах есть своё особое изящество, непринуждённый, расслабленный шик и тонкая чувственность. В каждой из них скрыта сексуальность и бесконечное очарование. Такая непринуждённость
и естественность всегда в моде! Как и знаменитые рубашки-«поло», ставшие фирменным стилем Дома Lacoste
и вдохновившие его на создание Eau De Lacoste - женской версии аромата, посвященного чистоте и свежести хлопчатобумажных тканей.</p>
        </div>

        <?php includeArea('catalog') ?>
    </div>
    
   

<?php includeArea('footer'); ?> 