$(function(){




$('.brand-list__alphabet-item').click(function(){
	$('.brand-list__alphabet-item').removeClass('brand-list__alphabet-item--current');
	$(this).addClass('brand-list__alphabet-item--current');
});




$('.catalog-list__preview').fancybox({'padding': 40}); 
$('.checkbox__inpt').customCheckbox();


$('.checkout__radio-inpt').customRadio();

$('.count__inpt').numeric();
$('.count__inpt').keyup(function(){
	if($(this).val() === '0') {
		$(this).val('1' + ' шт.');
	}
});
$('.count__inpt').focusout(function(){
	if($(this).val() === '') {
		$(this).val('1' + ' шт.');
	}
});
$('.count a').click(function(e){
	e.preventDefault();
	var defaultCount = parseInt($(this).parent('.count').find('.count__inpt').val());
	var activeEvent = $(this).attr('data-event');


	if(activeEvent == 'plus') {
		$(this).parent('.count').find('.count__inpt').val(defaultCount+1 + ' шт.');
	} else {
		if(defaultCount != 1) {
			$(this).parent('.count').find('.count__inpt').val((defaultCount-1) + ' шт.');
		}
	}
}); 


$('.filter__title').click(function(e){
	e.preventDefault();
	$(this).find('.filter__triangle').toggleClass('filter__triangle--close');
	$(this).next('.filter__hide').slideToggle(200);
});













$('.order-call').fancybox();





$('.product__gallery').pikachoose({
	carousel:true, 
	carouselVertical:true,
	showCaption:false
});






$('.sidebar-menu__triangle').click(function(){
	$(this).toggleClass('sidebar-menu__triangle--open');
	$(this).parents('li').children('.sidebar-menu__dropdown').slideToggle();
});
$('.slider').parfumSlider();

$('.sort__to-select').chosen({
	"disable_search": true,
	"width": '200px'
});




});
//# sourceMappingURL=script.js.map