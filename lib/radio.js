jQuery.fn.customRadio = function(options) {
    
    // Создаём настройки по-умолчанию
    var settings = $.extend( {
      'activeClass'         : 'icon-checked-radio'
    }, options);
    
    this.each(function() {
    	var item = $(this),
    		wrap = item.wrap('<div class="radio"></div>'),
    		itemStatus = item.attr('checked');
    	if(itemStatus == 'checked') {
    		item.after('<span class="radio__icon '+settings.activeClass+'"></span>');
    	} else {
    		item.after('<span class="radio__icon"></span>');
    	}

    	item.change(function(){
            var itemName = $(this).attr('name');
            $('.checkout__radio-inpt[name = '+itemName+']').next('.radio__icon').removeClass(settings.activeClass);
    		$(this).next('.radio__icon').addClass(settings.activeClass);
    	});
    });
}