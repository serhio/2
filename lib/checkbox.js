jQuery.fn.customCheckbox = function(options) {
    
    // Создаём настройки по-умолчанию
    var settings = $.extend( {
      'activeClass'         : 'icon-checked'
    }, options);
    
    this.each(function() {
    	var item = $(this),
    		wrap = item.wrap('<div class="checkbox"></div>'),
    		itemStatus = item.attr('checked');
    	if(itemStatus == 'checked') {
    		item.after('<span class="checkbox__icon '+settings.activeClass+'"></span>');
    	} else {
    		item.after('<span class="checkbox__icon"></span>');
    	}

    	item.change(function(){
    		$(this).next('.checkbox__icon').toggleClass(settings.activeClass);
    	});
    });
}