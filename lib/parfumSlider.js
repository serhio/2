jQuery.fn.parfumSlider = function(options) {
    
    // Создаём настройки по-умолчанию
    var settings = $.extend( {
      'speed'         : '5000',
      'animated'      : '500'
    }, options);
    
    this.each(function() {
        var ul = $('.slide__list'),
            items = ul.children('li'),
            curr = 0,
            beforeCurr =0,
            pagination = $('.slider__pagination li'),
            intervalId;
        
        items.eq(0).addClass('active');
        pagination.eq(0).addClass('active');
        pagination.eq(beforeCurr).find('.caption').addClass('bottom-slide').css('height', '100%');
        
        pagination.click(function(){
            beforeCurr = curr;
            curr = $(this).index();
            if(beforeCurr == curr) {
                return false;
            } else {
                nextSlide(curr);
                setActivePag(curr, beforeCurr);
                clearInterval(intervalId); 
                autoAnimation();
            }
        });
        
        
        
        var autoAnimation = function(){
            intervalId = setInterval(function(){
                beforeCurr = curr;
                curr = (curr < 3) ? curr + 1 : 0;
                nextSlide(curr);
                setActivePag(curr, beforeCurr);
            }, settings.speed);
        };
        
        autoAnimation(); 
        
        var setActivePag = function(curr, beforeCurr) {
                pagination.removeClass('active');
            
                $('.slider__caption-bg').animate({
                    top: 25 * curr + '%'
                }, 300, function(){
                    pagination.eq(curr).addClass('active');
                });

        };
        
        var nextSlide = function(curr) {
            items.fadeOut(settings.animated);
            items.eq(curr).fadeIn(settings.animated);
        };
    });
};