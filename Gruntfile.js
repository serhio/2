module.exports = function(grunt) {
    grunt.initConfig({
        concat: {
          options: {
            banner: '$(function(){\n',
            footer: '\n});',
            sourceMap: true
          },
          dist: {
            src: ['php/include/*/*.js'],
            dest: 'js/script.js',
          },
        },
        php2html: {
            default: {
              options: {
                htmlhint: false,
              },
              files: [
                {
                  expand: true,
                  cwd: 'php/',
                  src: ['*.php'],
                  dest: 'html',
                  ext: '.html'
                }
              ]
             }
        },
        less_imports: {
            options: {
                banner: "@import 'variable.less';\n@import 'style.less';",
                inlineCSS: false
            },
            your_target: {
                src: [ 'php/include/*/*.less'],
                dest: 'less/imports.less'
            }
        },
        sprite:{
          all: {
            padding: 5,
            src: 'php/include/*/img/*.png',
            destImg: 'img/spritesheet.png',
            destCSS: 'css/sprites.css'
          }
        },
        less: {
          development: {
            options: {
              paths: ["less"],
              sourceMap: true
            },
            files: {
              "css/result.css": "less/imports.less"
            }
          }  
        },
        watch: {
          options: {
              livereload: true   
          }, 
          scripts: {
              files: ['less/*.less', 'php/include/*/*', 'php/include/*/img/*', 'php/include/*/*.js', 'php/include/*.less', 'php/include/*/img/*.png', 'php/*', 'php/include/*'],
              tasks: ['newer:less_imports', 'newer:less', 'sprite', 'concat', 'php2html']
          }
        }
        
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-php2html');
    grunt.loadNpmTasks('grunt-less-imports');
    grunt.loadNpmTasks('grunt-newer');

    require('time-grunt')(grunt);
};